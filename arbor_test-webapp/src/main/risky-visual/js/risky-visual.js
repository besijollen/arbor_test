//
//  main.js
//
//  A project template for using arbor.js
//

(function($){

  var Renderer = function(canvas){
    var canvas = $(canvas).get(0)
    var ctx = canvas.getContext("2d");
    var particleSystem
    var activeNode
    var originNode
    var auxNode
    var spinner
    var damper

    var that = {
      init:function(system){
        //
        // the particle system will call the init function once, right before the
        // first frame is to be drawn. it's a good place to set up the canvas and
        // to pass the canvas size to the particle system
        //
        // save a reference to the particle system for use in the .redraw() loop
        particleSystem = system

        particleSystem.eachNode(function(node, pt){
          if (node.data.origin == true) {
            originNode = node;
          }
        })

        spinner = arbor.Point(1, 0)

        // inform the system of the screen dimensions so it can map coords for us.
        // if the canvas is ever resized, screenSize should be called again with
        // the new dimensions
        particleSystem.screenSize(canvas.width, canvas.height) 
        particleSystem.screenPadding(200) // leave an extra 80px of whitespace per side
        
        // set up some event handlers to allow for node-dragging
        that.initMouseHandling()
      },
      
      redraw:function(){
        // 
        // redraw will be called repeatedly during the run whenever the node positions
        // change. the new positions for the nodes can be accessed by looking at the
        // .p attribute of a given node. however the p.x & p.y values are in the coordinates
        // of the particle system rather than the screen. you can either map them to
        // the screen yourself, or use the convenience iterators .eachNode (and .eachEdge)
        // which allow you to step through the actual node objects but also pass an
        // x,y point in the screen's coordinate system
        // 
        ctx.fillStyle = "white"
        ctx.fillRect(0,0, canvas.width, canvas.height)
        that.spinSpinner(0.1)
        damper = damper * 0.995
        
        particleSystem.eachEdge(function(edge, pt1, pt2){
          // edge: {source:Node, target:Node, length:#, data:{}}
          // pt1:  {x:#, y:#}  source position in screen coords
          // pt2:  {x:#, y:#}  target position in screen coords

          // draw a line from pt1 to pt2
          ctx.strokeStyle = edge.data.badness > 80 ? "rgba(255,0,0, 1)" : edge.data.badness > 20 ? "rgba(255,165,0, 1)" : "rgba(11,102,35, 1)"
          ctx.lineWidth = 2
          ctx.beginPath()
          ctx.moveTo(pt1.x, pt1.y)
          ctx.lineTo(pt2.x, pt2.y)
          ctx.stroke()
        })

        particleSystem.eachNode(function(node, pt){
          // node: {mass:#, p:{x,y}, name:"", data:{}}
          // pt:   {x:#, y:#}  node position in screen coords

          // draw a rectangle centered at pt
          var active = node == activeNode

          //that.rotate(originNode, 0.01)

          if (node == originNode) {
            var w = 50
            //originNode.p = originNode.p.add(spinner.multiply(0.01))
            ctx.fillStyle = "blue"
            ctx.fillRect(pt.x-w*2, pt.y-w/2, w*4,w)
            ctx.fillStyle = "white"
            ctx.fillText(active ? node.data.info : node.name, pt.x - w*1.5, pt.y)
          } else if (active) {
            //node.p = node.p.multiply(0.997)
            //that.rotate(node, 0.01)
            //node.p = node.p.add(arbor.Point(spinner.x, 0).multiply(0.01 * damper))
            //node.p = node.p.multiply(1 + (spinner.x * damper / node.p.magnitude()))
            node.p = node.p.add(arbor.Point(0, spinner.x * damper))
            var w = 50
            ctx.fillStyle = node.data.badness > 80 ? "red" : node.data.badness > 20 ? "orange" : "green"
            ctx.fillRect(pt.x-w*2, pt.y-w/2, w*4,w)
            ctx.fillStyle = "white"
            ctx.fillText(node.data.info, pt.x - w*1.5, pt.y)
          } else {
            var w = 25
            ctx.fillStyle = node.data.badness > 80 ? "red" : node.data.badness > 20 ? "orange" : "green"
            ctx.fillRect(pt.x-w*2, pt.y-w/2, w*4,w)
            ctx.fillStyle = "white"
            ctx.fillText(node.name, pt.x - w*1.5, pt.y)
          }
        })
      },

      rotate:function(node, angle){
        x = node.p.x
        y = node.p.y

        X = x * Math.cos(angle) - y * Math.sin(angle)
        Y = x * Math.sin(angle) + y * Math.cos(angle)

        node.p = arbor.Point(X, Y)
      },

      spinSpinner:function(angle){
        x = spinner.x
        y = spinner.y

        X = x * Math.cos(angle) - y * Math.sin(angle)
        Y = x * Math.sin(angle) + y * Math.cos(angle)

        spinner = arbor.Point(X, Y)
      },
      
      initMouseHandling:function(){
        // no-nonsense drag and drop (thanks springy.js)
        var dragged = null;

        // set up a handler object that will initially listen for mousedowns then
        // for moves and mouseups while dragging
        var handler = {
          clicked:function(e){
            var pos = $(canvas).offset();
            _mouseP = arbor.Point(e.pageX-pos.left, e.pageY-pos.top)
            dragged = particleSystem.nearest(_mouseP);
            if (activeNode != dragged.node) {
                activeNode = dragged.node;
                spinner = arbor.Point(-1, 0) // activeNode.p.normalize()
                damper = 0.01
            } else {
                activeNode = null;
            }

            if (dragged && dragged.node !== null){
              // while we're dragging, don't let physics move the node
              dragged.node.fixed = true
            }

            $(canvas).bind('mousemove', handler.dragged)
            $(window).bind('mouseup', handler.dropped)

            return false
          },
          dragged:function(e){
            var pos = $(canvas).offset();
            var s = arbor.Point(e.pageX-pos.left, e.pageY-pos.top)

            if (dragged && dragged.node !== null){
              var p = particleSystem.fromScreen(s)
              dragged.node.p = p
            }

            return false
          },

          dropped:function(e){
            if (dragged===null || dragged.node===undefined) return
            if (dragged.node !== null) dragged.node.fixed = false
            dragged.node.tempMass = 1000
            dragged = null
            $(canvas).unbind('mousemove', handler.dragged)
            $(window).unbind('mouseup', handler.dropped)
            _mouseP = null
            return false
          }
        }
        
        // start listening
        $(canvas).mousedown(handler.clicked);

      },
      
    }
    return that
  }    

  $(document).ready(function(){
    var sys = arbor.ParticleSystem(200, 800, 0) // create the system with sensible repulsion/stiffness/friction
    sys.parameters({gravity:true}) // use center-gravity to make the graph settle nicely (ymmv)
    sys.renderer = Renderer("#viewport") // our newly created renderer will have its .init() method called shortly by sys...

    $.getJSON("data/test4.json",function(data){
              // load the raw data into the particle system as is (since it's already formatted correctly for .merge)
              var nodes = data.nodes

              sys.merge({nodes:nodes, edges:data.edges})
            })
  })

})(this.jQuery)
